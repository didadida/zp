function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var wheel = document.getElementById('wheel');
var popup = {
  el: document.getElementById('popup'),
  show: function show(o) {
    this.el.querySelector(".mui-media-body").innerHTML = "<h3>".concat(o.title || "", "</h3><h4>").concat(o.content || "", "</h4><h5><a href=\"").concat(o.href || "", "\">\u70B9\u51FB\u9886\u53D6</a></h5>");
    this.el.className += " popup-in";
  },
  close: function close() {
    this.el.className = "popup";
    wheel.style.transform = wheel.style.transform.replace(/\d+\.*\d*/g, function (a, b, c) {
      return a % 360;
    });
    wheel.style.webkitTransform = wheel.style.webkitTransform.replace(/\d+\.*\d*/g, function (a, b, c) {
      return a % 360;
    });
  }
};
var action = window.action = {
  items: _toConsumableArray(wheel.getElementsByClassName('item')),
  list: [],
  wheelWidth: wheel.offsetWidth,
  point: document.getElementById('point'),
  marquee: document.getElementById('win-list'),
  count: $(".count span"),
  initItems: function initItems() {
    var _this = this;

    var angle = 360 / this.items.length;
    var r = this.wheelWidth / 2; //半径

    var r2 = r - this.wheelWidth * 0.08;
    this.items.forEach(function (v, i) {
      var an = angle * i;
      var l = r + r2 * Math.sin((an + angle / 2) * Math.PI / 180);
      var t = r + r2 * -Math.cos((an + angle / 2) * Math.PI / 180) - 10;
      v.style.left = l + 'px';
      v.style.top = t + 'px';
      v.style.transform = v.style.webkitTransform = "rotate(".concat(115 + an, "deg) scale(.9)");

      _this.list.push({
        probability: +v.dataset.probability,
        angle: an + angle / 2,
        prizeId: v.dataset.id,
        prizeName: v.innerHTML
      });
    });
    var n = 0;
    this.list.sort(function (a, b) {
      return a.probability > b.probability;
    }).forEach(function (v) {
      v.probability = n += v.probability;
    });
    this.point.style.cssText = "margin-left:-".concat(this.point.offsetWidth / 2, ";margin-top:-").concat(this.point.offsetHeight / 2);
  },
  setCount: function setCount(n) {
    this.count.html(n);
  },
  reuslt: function reuslt(target) {
    popup.show({
      title: "恭喜获得" + target.prizeName + "id是" + target.prizeId,
      content: "已连续转动100天",
      href: "http://www.baidu.com"
    });
  },
  start: function start(button) {
    var n = Math.ceil(Math.random() * this.list.reduce(function (sum, v) {
      return sum + v.probability * 1;
    }, 0));
    var target = this.list.filter(function (v) {
      return v.probability >= n;
    })[0];
    var angle = target.angle;
    var time = 3 + Math.ceil(Math.random() * 5);
    button.className = 'disabled';
    wheel.style.transform = wheel.style.webkitTransform = "rotate(".concat(360 * (10 + Math.ceil(Math.random() * 10)) - angle, "deg)");
    wheel.style.transitionDuration = wheel.style.webikitTransitionDuration = time + "s";
    setTimeout(function () {
      wheel.style.transitionDuration = wheel.style.webkitTransitionDuration = 0 + "s";
      button.removeAttribute('class');
      this.reuslt(target);
    }.bind(this), time * 1000 + 1000);
  },
  bindEvt: function bindEvt() {
    var self = this;

    this.point.onclick = function () {
      //此处校验可抽奖次数。可以则调用start
      // $.error('抽奖次数已用尽')
      //设置抽奖次数文字设置 self.setCount(100)
      self.start(this);
    };
  },
  initMarquee: function initMarquee() {
    var inner = this.marquee.children[0];
    var h1 = this.marquee.offsetHeight;
    var h2 = inner.offsetHeight;
    if (h2 < h1) return inner.style.animationPlayState = inner.style.webkitAnimationPlayState = "paused";
    inner.style.animationDuration = inner.style.webkitAnimationDuration = inner.children.length * 2 + "s";
    inner.innerHTML += inner.innerHTML;
  },
  init: function init() {
    this.initItems();
    this.bindEvt();
    this.initMarquee();
  }
};
action.init();