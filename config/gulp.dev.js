const gulp = require('gulp')
const sass = require('gulp-sass')
const less = require('gulp-less')
const gulpIf = require('gulp-if')
const concat = require('gulp-concat')
const plumber = require('gulp-plumber')
const notify = require('gulp-notify')
const include = require('gulp-file-include')
const changed = require('gulp-changed')
const replace = require('gulp-replace')
const browserSync = require('browser-sync').create()
const reload = browserSync.reload
const run = require('run-sequence')
const through = require('through2')

const config = require('./gulp.config')
const cssPrep = config.cssPrep
const mergeFile = config.js.mergeTarget.map(v => config.js.src.replace("**", v))
const cssMergeAll = config.css.mergeTarget === "*"

const handlerHTML = flag => {
    const dir = config.html
    return gulp.src(dir.src)
        .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
        .pipe(flag ? changed(dir.dist) : through.obj())
        .pipe(include({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest(dir.dist))
        .pipe(reload({stream: true}))
}
module.exports = () => {
    gulp.task('html', e => handlerHTML(true))
    gulp.task('components', e => handlerHTML(false))
    gulp.task(cssPrep, e => {
        const dir = config.css
        return gulp.src(dir.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
            .pipe(changed(dir.dist))
            .pipe(gulpIf(f => config.css.lib.find(v => v !== f.relative), eval(cssPrep)()))
            .pipe(gulpIf(f => cssMergeAll ? config.css.lib.find(v => v === f.relative) ? false : true : config.css.mergeTarget.find(v => v === f.relative), concat(config.css.mergeName)))
            .pipe(gulp.dest(dir.dist))
            .pipe(reload({stream: true}))
    })
    gulp.task('jsMerge', e => {
        const dir = config.js
        return gulp.src(mergeFile)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
            .pipe(changed(dir.dist))
            .pipe(replace(config.placeUrl.key, config.placeUrl.devUrl))
            .pipe(concat(dir.mergeName))
            .pipe(gulp.dest(dir.dist))
            .pipe(reload({stream: true}))
    })
    gulp.task('js', e => {
        const dir = config.js
        return gulp.src([dir.src].concat(mergeFile.map(v => '!' + v)))
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
            .pipe(changed(dir.dist))
            .pipe(replace(config.placeUrl.key, config.placeUrl.devUrl))
            .pipe(gulp.dest(dir.dist))
            .pipe(reload({stream: true}))
    })
    gulp.task('copy', e => gulp.src(config.assets.src)
        .pipe(changed(config.assets.dist))
        .pipe(gulp.dest(config.assets.dist))
        .pipe(reload({stream: true}))
    )
    gulp.task('default', () => {
        run('html', 'components', cssPrep, 'jsMerge', 'js', 'copy')
        gulp.watch(config.html.src, ['html'])
        gulp.watch(config.components.src, ['components'])
        gulp.watch(config.css.src, [cssPrep])
        gulp.watch(config.js.src, ['jsMerge', 'js'])
        gulp.watch(config.assets.src, ['copy'])
        browserSync.init({
            server: {
                baseDir: config.dist
            }, notify: true
        })
    })
}