const gulp = require('gulp')
const sass = require('gulp-sass')
const less = require('gulp-less')
const gulpIf = require('gulp-if')
const concat = require('gulp-concat')
const plumber = require('gulp-plumber')
const notify = require('gulp-notify')
const include = require('gulp-file-include')
const autoprefixer = require('gulp-autoprefixer')
const cssnano = require('gulp-cssnano')
const uglify = require('gulp-uglify')
const imagemin = require('gulp-imagemin')
const rev = require('gulp-rev')
const revCollector = require('gulp-rev-collector')
const babel = require('gulp-babel')
const replace = require('gulp-replace')
const htmlmin = require('gulp-htmlmin')

const del = require('del')
const run = require('run-sequence')

const config = require('./gulp.config')
const cssPrep = config.cssPrep
const mergeFile = config.js.mergeTarget.map(v => config.js.src.replace("**", v))
const cssMergeAll = config.css.mergeTarget === "*"

const options = {
    removeComments: true,//清除HTML注释
    collapseWhitespace: true,//压缩HTML
    // collapseBooleanAttributes: true,//省略布尔属性的值 <input checked="true"/> ==> <input />
    removeEmptyAttributes: true,//删除所有空格作属性值 <input id="" /> ==> <input />
    removeScriptTypeAttributes: true,//删除<script>的type="text/javascript"
    removeStyleLinkTypeAttributes: true,//删除<style>和<link>的type="text/css"
    minifyJS: true,//压缩页面JS
    minifyCSS: true//压缩页面CSS
}

module.exports = () => {
    gulp.task(cssPrep, e => {
        const dir = config.css
        return gulp.src(dir.src)
          .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
          .pipe(gulpIf(f => config.css.lib.find(v => v !== f.relative), eval(cssPrep)()))
          .pipe(gulpIf(f => cssMergeAll ? config.css.lib.find(v => v === f.relative) ? false : true : config.css.mergeTarget.find(v => v === f.relative), concat(config.css.mergeName)))
          .pipe(gulpIf(f => config.css.compress && !config.css.lib.find(v => v === f.relative), cssnano()))
          .pipe(gulpIf(f => config.version && !config.css.lib.find(v => v === f.relative), rev()))
          .pipe(autoprefixer({browsers: ['safari 5', 'ios 6', 'android 4']}))
          .pipe(gulp.dest(dir.dist))
          .pipe(gulpIf(f => config.version, rev.manifest('css.json')))
          .pipe(gulp.dest(config.rev))
    })
    gulp.task('jsMerge', e => {
        const dir = config.js
        return gulp.src(mergeFile)
          .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
          .pipe(gulpIf(f => !config.js.lib.find(v => v === f.relative), babel({
              presets: ['@babel/env'],
          })))
          .pipe(gulpIf(f => !config.js.lib.find(v => v !== f.relative), replace(config.placeUrl.key, config.placeUrl.buildUrl)))
          .pipe(gulpIf(f => config.js.compress && config.js.lib.find(v => v !== f.relative), uglify({
              mangle: {reserved: config.js.reserved}
          })))
          .pipe(concat(dir.mergeName))
          .pipe(gulpIf(f => config.js.compress, uglify({
              mangle: {reserved: config.js.reserved}
          })))
          .pipe(gulpIf(f => config.version && !config.js.lib.find(v => v === f.relative), rev()))
          .pipe(gulp.dest(dir.dist))
          .pipe(gulpIf(f => config.version, rev.manifest('jsMerge.json')))
          .pipe(gulp.dest(config.rev))
    })
    gulp.task('js', e => {
        const dir = config.js
        return gulp.src([dir.src].concat(mergeFile.map(v => '!' + v)))
          .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
          .pipe(gulpIf(f => !config.js.lib.find(v => v === f.relative), babel({
              "presets": [
                  ["@babel/env", {
                      "modules": false,
                      "targets": {
                          "browsers": "> 1%"
                      }
                  }]
              ]
          })))
          .pipe(gulpIf(f => config.js.lib.find(v => v === f.relative), replace(config.placeUrl.key, config.placeUrl.buildUrl)))
          .pipe(gulpIf(f => config.js.compress && !config.js.lib.find(v => v === f.relative), uglify({
              mangle: {reserved: config.js.reserved}
          })))
          .pipe(gulpIf(f => config.version && !config.js.lib.find(v => v === f.relative), rev()))
          .pipe(gulp.dest(dir.dist))
          .pipe(gulpIf(f => config.version, rev.manifest('js.json')))
          .pipe(gulp.dest(config.rev))

    })
    gulp.task('images', e => gulp.src(config.images.src)
      .pipe(imagemin({
          optimizationLevel: 3
          , progressive: true
          , interlaced: true
      }))
      .pipe(gulp.dest(config.images.dist))
    )
    gulp.task('copy', e => gulp.src([config.assets.src].concat(config.assets.exclude))
      .pipe(gulp.dest(config.assets.dist))
    )
    gulp.task('rev', e => gulp
      .src([config.rev + '/*.json', config.html.src])
      .pipe(include({
          prefix: '@@',
          basepath: '@file'
      }))
      .pipe(gulpIf(f => config.html.compress, htmlmin(options)))
      .pipe(revCollector({
          replaceReved: true
      }))
      .pipe(gulp.dest(config.html.dist))
    )
    gulp.task('default', () => {
        del(config.dist).then(rs => run(cssPrep, 'jsMerge', 'js', 'images', 'copy', 'rev', done => {
            del(config.rev)
        }))
        // del(config.dist).then(rs => run('copy'))
    })
}