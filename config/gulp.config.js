const src = "./src" //源码目录
const dist = "./dist"   //构建目录
const rev = "./rev" //版本目录
const cssPrep = 'sass'  //预处理器 less sass

module.exports = {
    src, //源码目录
    dist, //构建目录
    rev, //版本目录
    cssPrep,
    placeUrl: {
        key: '__placeholder__', //占位
        devUrl: 'http://www.baidu.com', //开发地址
        buildUrl: 'http://www.jd.com', //生产地址
    },
    version: true, //是否开启版本控制
    html: {
        src: src + "/**/*.html",  //html源目录
        dist: dist, //html生产目录,
        compress: false, //是否压缩html 只在生产生效
    },
    components: {
        src: src + "/components/**", //组建目录 存放比如header footer等公用html
    },
    css: {
        src: src + "/css/**",  //css开发源目录 。 放置css less sass 等文件，less sass已经内置，styl文件需要自行安装gulp-stylus 并更改cssPrep
        dist: dist + '/css',    //css构建目录
        mergeTarget: "*", // "*" 表示全部合并 . [a.css,b.scss]定向合并文件. []不合并任何
        mergeName: 'app.min.css',   //合并后的名称
        lib: ['mui.min.css'],//第三方库，不参与任何处理（也不参与版本标记）
        compress: true  //是否压缩css，只在生产生效
    },
    js: {
        src: src + "/js/**",    //JS源目录
        dist: dist + "/js", //js构建目录
        mergeTarget: ["zepto.js", "util.js"], // 要合并的文件，通常是第三方库及其扩展.不支持*全部
        mergeName: 'vendor.js',  //合并后的名字
        lib: ['zepto.js', 'mui.min.js', 'jquery.min.js'], //不参编译 版本标记 合并 ， 参与压缩
        compress: false, //是否开启压缩，只在生产生效
        reserved: ['$', "exports", "module", "require", "define", "mui"]  //压缩混淆时需要忽略的key
    },
    images: {
        src: src + "/assets/images/**", //图片源目录，
        dist: dist + "/assets/images" //图片构建目录。。默认开启image-min图片压缩
    },
    //静态资源复制
    assets: {
        src: src + "/assets/**", //其他静态资源源目录
        exclude: '!' + src + "/assets/images/**", //图片因为执行了images任务。此处忽略
        dist: dist + "/assets" //其他静态资源构建目录。
    }
}