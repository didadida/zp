const cutTime = 30
const action = window.action = {
    forms: $("input"),
    sendCode: $("form span"),
    imgCode: $("form img"),
    sendFormEL: $("form button"),
    cutTime,
    timer: null,
    cutDown() {
        if (this.cutTime === 0) {
            clearInterval(this.timer)
            this.sendCode.removeClass("disabled")
            this.sendCode.html("发送验证码")
            this.cutTime = cutTime
        } else {
            this.sendCode.html(this.cutTime + "s")
            this.cutTime--
        }
    },
    sendCodeEntry() {
        clearInterval(this.timer)
        this.sendCode.addClass("disabled")
        this.cutDown()
        this.timer = setInterval(this.cutDown.bind(this), 1000)
    },
    changeImg() {
        alert(1)
        // this.imgCode.src=""   //点击更换验证码图片
    },
    initEvt() {
        this.imgCode.click(this.changeImg.bind(this))
        this.sendCode.click(this.sendCodeEntry.bind(this))
        this.sendFormEL.click(this.go.bind(this))
    },
    go() {
        let outerFlag = true
        let info={}
        this.forms.each((i, v) => {
            let flag
            if (v.dataset.repeat) {
                flag = v.value === $("[type=password]").eq(0).val()
            } else if (v.dataset.regexp) {
                flag = new RegExp(v.dataset.regexp).test($(v).val())
            } else {
                flag = $(v).prop("checked")
            }
            if (!flag) {
                $.error(v.dataset.message)
                outerFlag = flag
                return false
            }
            info[v.name] = v.value
        })
        if(outerFlag){
            this.sendForm(info)
        }
    },
    sendForm(info){
        console.error(info)
    },
    init() {
        this.initEvt()
    }
}
action.init()

