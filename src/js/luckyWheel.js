const wheel = document.getElementById('wheel')
const popup = {
    el: document.getElementById('popup'),
    show(o) {
        this.el.querySelector(".mui-media-body").innerHTML = `<h3>${o.title || ""}</h3><h4>${o.content || ""}</h4><h5><a href="${o.href || ""}">点击领取</a></h5>`
        this.el.className += " popup-in"
    },
    close() {
        this.el.className = "popup"
        wheel.style.transform = wheel.style.transform.replace(/\d+\.*\d*/g, (a, b, c) => a % 360)
        wheel.style.webkitTransform = wheel.style.webkitTransform.replace(/\d+\.*\d*/g, (a, b, c) => a % 360)
    }
}
const action = window.action = {
    items: [...wheel.getElementsByClassName('item')],
    list: [],
    wheelWidth: wheel.offsetWidth,
    point: document.getElementById('point'),
    marquee: document.getElementById('win-list'),
    count: $(".count span"),
    initItems() {
        const angle = 360 / this.items.length
        const r = this.wheelWidth / 2  //半径
        const r2 = r - this.wheelWidth * 0.08
        this.items.forEach((v, i) => {
            const an = angle * i
            const l = r + r2 * Math.sin((an + angle / 2) * Math.PI / 180)
            let t = r + r2 * -Math.cos((an + angle / 2) * Math.PI / 180) - 10
            v.style.left = l + 'px'
            v.style.top = t + 'px'
            v.style.transform = v.style.webkitTransform = `rotate(${115 + an}deg) scale(.9)`
            this.list.push({
                probability: +v.dataset.probability,
                angle: an + angle / 2,
                prizeId: v.dataset.id,
                prizeName: v.innerHTML
            })
        })
        let n = 0
        this.list.sort((a, b) => a.probability > b.probability).forEach(v => {
            v.probability = n += v.probability
        })
        this.point.style.cssText = `margin-left:-${this.point.offsetWidth / 2};margin-top:-${this.point.offsetHeight / 2}`
    },
    setCount(n) {
        this.count.html(n)
    },
    reuslt(target) {
        popup.show({
            title: "恭喜获得" + target.prizeName + "id是" + target.prizeId,
            content: "已连续转动100天",
            href: "http://www.baidu.com"
        })
    },
    start(button) {
        const n = Math.ceil(Math.random() * this.list.reduce((sum, v) => sum + v.probability * 1, 0))
        const target = this.list.filter(v => v.probability >= n)[0]
        const angle = target.angle
        const time = 3 + Math.ceil(Math.random() * 5)
        button.className = 'disabled'
        wheel.style.transform = wheel.style.webkitTransform = `rotate(${360 * (10 + Math.ceil(Math.random() * 10)) - angle}deg)`
        wheel.style.transitionDuration = wheel.style.webikitTransitionDuration = time + "s"
        setTimeout(function () {
            wheel.style.transitionDuration = wheel.style.webkitTransitionDuration = 0 + "s"
            button.removeAttribute('class')
            this.reuslt(target)
        }.bind(this), time * 1000 + 1000)
    },
    bindEvt() {
        const self = this
        this.point.onclick = function () {
            //此处校验可抽奖次数。可以则调用start
            // $.error('抽奖次数已用尽')
            //设置抽奖次数文字设置 self.setCount(100)
            self.start(this)
        }
    },
    initMarquee() {
        const inner = this.marquee.children[0]
        const h1 = this.marquee.offsetHeight
        let h2 = inner.offsetHeight
        if (h2 < h1) return inner.style.animationPlayState = inner.style.webkitAnimationPlayState = "paused"
        inner.style.animationDuration = inner.style.webkitAnimationDuration = inner.children.length * 2 + "s"
        inner.innerHTML += inner.innerHTML
    },
    init() {
        this.initItems()
        this.bindEvt()
        this.initMarquee()
    }
}
action.init()

